Paseo por los árboles de Madrid
-------------------------------

Indice

1. Introducción

2. Generación del paseo

3. Código

4. Créditos

--------------------------------------

1. Introducción

En este libro, el algoritmo de las cadenas de Markov genera simultáneamente un poema y un paseo por los árboles del barrio de Las Letras, en el centro de Madrid. A pesar de la impresión de que hay pocos árboles en el barrio, el algoritmo cuenta con 460 de ellos. 

La cadena de Markov fue diseñada en 1906 por Andrey Markov, matemático ruso fallecido en 1992. Este algoritmo está en la base de muchos programas informáticos que generan spam. Se utiliza para sistemas que describen una serie de eventos que son interdependientes. Lo que ocurre depende únicamente del paso anterior. Por eso las cadenas de Markov también se denominan "sin memoria".

Para generar el texto, la cadena de Markov analiza la secuencia de palabras de una novela. En esta ocasión, el lector puede elegir entre un fragmento de la novela "La Madre Naturaleza" de la escritora feminista del siglo XIX Emilia Pardo Bazán; o un fragmento de la novela "Miau" del escritor madrileño del siglo XIX Benito Pérez Gáldos, fallecido hace exactamente 100 años en el momento de la escritura.

El nuevo texto se compone de palabras significativas (sustantivos, verbos, adjetivos), cada una de las cuales está vinculada a un árbol concreto. El camino que se recorre de árbol a árbol se traduce en el poema por las pequeñas palabras del libro. El poema y, por tanto, el recorrido se generan por partes, de modo que se puede leer una línea del poema a cada árbol. El último árbol tiene entonces la suerte de escuchar el poema completo.

Paseo por los árboles de Madrid es un libro en la 'Editorial Algoliteraria: crear alianzas con los árboles'.
El libro es por definición infinito y cada copia es única.

Anaïs Berck es un seudónimo y representa una colaboración entre humanos, algoritmos y árboles. Anaïs Berck explora las especificidades de la inteligencia humana en medio de las inteligencias artificiales y vegetales. Este libro fue creado por Jaime Munárriz, Luis Morell, An Mertens, Eva Marina Gracia, Gijs de Heij, Ana Isabel Garrido Mártinez, Alfredo Calosci y Daniel Arribas Hedo durante una residencia en Medialab Prado en Madrid. La residencia fue concedida por el programa "Residencia Cultura Digital" iniciado por el Gobierno Flamenco.

En esta obra Anaïs Berck es representadx por:
- el algoritmo La Cadena de Markov
- los árboles registrados por la Municpalidad de Madrid en el Barrio de Las Letras, colleción descargada desde el proyecto Un Alcorque, un Arbol: http://www-2.munimadrid.es/DGPVE_WUAUA/welcome.do
- los seres humanos Jaime Munárriz, Luis Morell, An Mertens, Eva Marina Gracia, Gijs de Heij, Ana Isabel Garrido Mártinez, Alfredo Calosci y Daniel Arribas Hedo

https://www.anaisberck.be/
https://constantvzw.org/
http://osp.kitchen/
https://algolit.net

------------------------------------

2. Generación del paseo


----------------------------

3. Código


----------------------------

4. Créditos

Este libro es una creación de Anaïs Berck para la 'Editorial Algoliteraria: crear alianzas con los árboles'. Vio la luz durante una residencia en Medialab Prado, Madrid, en junio de 2021, concedida por el Gobierno de Flandes en el programa 'Residency Digital Culture'.

El ejemplar de este libro es único y el número de copias impresas es por definición infinito.

Este ejemplar es el número xxxx de los ejemplares descargados.

Collective Conditions for (re-)use (CC4r), 2021
Copyleft con una diferencia: Se le invita a copiar, distribuir y modificar esta obra bajo los términos de la CC4r: https://gitlab.constantvzw.org/unbound/cc4r
