# Scripts

$ paseo.py

# Data

Projection used by the api is [EPSG:25830](https://epsg.io/)25830

Convert betwenn lat/long and this projection with here: <https://epsg.io/transform#s_srs=4326&t_srs=25830>

Retreive a 1000 trees within the bounding 
<http://sigma.madrid.es/arcgismalla/rest/services/MTMOV/MPUAUA/MapServer/0/query?f=geojson&where=&returnGeometry=true&spatialRel=esriSpatialRelIntersects&geometry={%22xmin%22%3A440315.95%2C%22ymin%22%3A4473156.10%2C%22xmax%22%3A441571.52%2C%22ymax%22%3A4474261.62}&geometryType=esriGeometryEnvelope&outFields=*>

Ordered, with offset, to retreive more than a 1000 trees:
<http://sigma.madrid.es/arcgismalla/rest/services/MTMOV/MPUAUA/MapServer/0/query?f=geojson&where=&returnGeometry=true&spatialRel=esriSpatialRelIntersects&geometry={%22xmin%22%3A440315.95%2C%22ymin%22%3A4473156.10%2C%22xmax%22%3A441571.52%2C%22ymax%22%3A4474261.62}&geometryType=esriGeometryEnvelope&outFields=*&orderByFields=OBJECTID&resultOffset=0> increment the resultOffset each time with a 1000


general information on the map server: <http://sigma.madrid.es/arcgismalla/rest/services/MTMOV/MPUAUA/MapServer>

API Reference: <https://sigma.madrid.es/arcgismalla/sdk/rest/index.html#/Query_Feature_Service_Layer/02ss0000002r000000/>