The sun had set. After the brief interval of twilight the night fell
calm and dark, and in its gloomy bosom the last sounds of a sleepy
world died gently away. The traveller went forward on his way,
hastening his step as night came on; the path he followed was narrow
and worn by the constant tread of men and beasts, and led gently up a
hill on whose verdant slopes grew picturesque clumps of wild cherry
trees, beeches and oaks.--The reader perceives that we are in the north
of Spain.

Our traveller was a man of middle age, strongly built, tall and
broad-shouldered; his movements were brisk and resolute, his step
firm, his manner somewhat rugged, his eye bold and bright; his pace
was nimble, considering that he was decidedly stout, and he was--the
reader may at once be told, though somewhat prematurely--as good a
soul as you may meet with anywhere. He was dressed, as a man in easy
circumstances should be dressed for a journey in spring weather, with
one of those round shady hats, which, from their ugly shape, have been
nicknamed mushrooms (_hongo_), a pair of field-glasses hanging to a
strap, and a knotted stick which, when he did not use it to support his
steps, served to push aside the brambles when they flung their thorny
branches across so as to catch his dress.

He presently stopped, and gazing round the dim horizon, he seemed vexed
and puzzled. He evidently was not sure of his way and was looking
round for some passing native of the district who might give him such
topographical information as might enable him to reach his destination.

"I cannot be mistaken," he said to himself. "They told me to cross the
river by the stepping-stones--and I did so--then to walk on, straight
on. And there, to my right, I do in fact, see that detestable town
which I should call _Villafangosa_ by reason of the enormous amount of
mud that chokes the streets.--Well then, I can but go 'on, straight
on'--I rather like the phrase, and if I bore arms, I would adopt it
for my motto--in order to find myself at last at the famous mines of
Socartes."

But before he had gone much farther, he added: "I have lost my way,
beyond a doubt I have lost my way.--This, Teodoro Golfin, is the
result of your 'on, straight on.' Bah! these blockheads do not know
the meaning of words; either they meant to laugh at you or else
they did not know the way to the mines of Socartes. A huge mining
establishment must be evident to the senses, with its buildings and
chimneys, its noise of hammers and snorting of furnaces, neighing of
horses and clattering of machinery--and I neither see, nor hear, nor
smell anything. I might be in a desert! How absolutely solitary! If I
believed in witches, I could fancy that Fate intended me this night to
have the honor of making acquaintance with some. Deuce take it! why is
there no one to be seen in these parts? And it will be half an hour
yet before the moon rises. Ah! treacherous Luna, it is you who are to
blame for my misadventure.--If only I could see what sort of place I
am in.--However, what could I expect?" and he shrugged his shoulders
with the air of a vigorous man who scorns danger. "What, Golfin, after
having wandered all round the world are you going to give in now? The
peasants were right after all: 'on, straight on.' The universal law of
locomotion cannot fail me here."

And he bravely set out to test the law, and went on about a kilometre
farther, following the paths which seemed to start from under his feet,
crossing each other and breaking off at a short distance, in a thousand
angles which puzzled and tired him. Stout as his resolution was, at
last he grew weary of his vain efforts. The paths, which had at first
all led upwards, began to slope downwards as they crossed each other,
and at last he came to so steep a slope that he could only hope to get
to the bottom by rolling down it.

"A pretty state of things!" he exclaimed, trying to console himself for
this provoking situation by his sense of the ridiculous. "Where have
you got to now my friend? This is a perfect abyss. Is anything to be
seen at the bottom. No, nothing, absolutely nothing--the hill-side has
disappeared, the earth has been dug away. There is nothing to be seen
but stones and barren soil tinged red with iron. I have reached the
mines, no doubt of that--and yet there is not a living soul to be seen,
no smoky chimneys; no noise, not a train in the distance, not even a
dog barking. What am I to do? Out there the path seems to slope up
again.--Shall I follow that? Shall I leave the beaten track? Shall I go
back again? Oh! this is absurd! Either I am not myself or I will reach
Socartes to-night, and be welcomed by my worthy brother! 'On, straight
on.'"

He took a step, and his foot sank in the soft and crumbling soil.
"What next, ye ruling stars? Am I to be swallowed up alive? If only
that lazy moon would favor us with a little light we might see each
other's faces--and, upon my soul, I can hardly expect to find Paradise
at the bottom of this hole. It seems to be the crater of some extinct
volcano.... Nothing could be easier than a slide down this beautiful
precipice. What have we here?... A stone; capital--a good seat while I
smoke a cigar and wait for the moon to rise."

The philosophical Golfin seated himself as calmly as if it were a
bench by a promenade, and was preparing for his smoke, when he heard a
voice--yes, beyond a doubt, a human voice, at some little distance--a
plaintive air, or to speak more accurately, a melancholy chant of a
single phrase, of which the last cadence was prolonged into a "dying
fall," and which at last sank into the silence of the night, so softly
that the ear could not detect when it ceased.

"Come," said the listener, well pleased, "there are some human beings
about. That was a girl's voice; yes, certainly a girl's, and a lovely
voice too. I like the popular airs of this country-side. Now it has
stopped.... Hark! it will soon begin again.... Yes, I hear it once
more. What a beautiful voice, and what a pathetic air! You might
believe that it rose from the bowels of the earth, and that Señor
Golfin, the most matter-of-fact and least superstitious man in this
world, was going to make acquaintance with sylphs, nymphs, gnomes,
dryads, and all the rabble rout that obey the mysterious spirit of the
place.--But, if I am not mistaken, the voice is going farther away--the
fair singer is departing.... Hi, girl, child, stop--wait a minute!..."

The voice which had for a few minutes so charmed the lost wanderer with
its enchanting strains was dying away in the dark void, and at the
shouts of Golfin it was suddenly silent. Beyond a doubt the mysterious
gnome, who was solacing its underground loneliness by singing its
plaintive loves, had taken fright at this rough interruption by a human
being, and fled to the deepest caverns of the earth, where precious
gems lay hidden, jealous of their own splendor.

"This is a pleasant state of things--" muttered Golfin, thinking that
after all he could do no better than light his cigar.--"There seems no
reason why it should not go on for a hundred years. I can smoke and
wait. It was a clever idea of mine that I could walk up alone to the
mines of Socartes. My luggage will have got there before me--a signal
proof of the advantages of 'on, straight on.'"

A light breeze at this instant sprang up, and Golfin fancied he
heard the sound of footsteps at the bottom of the unknown--or
imaginary--abyss before him; he listened sharply, and in a minute felt
quite certain that some one was walking below. He stood up and shouted:

"Girl, man, or whoever you are, can I get to the mines of Socartes by
this road?"

He had not done speaking when he heard a dog barking wildly, and then a
manly voice saying: "Choto, Choto! come here!"

"Hi there!" cried the traveller. "My good friend--man, boy, demon, or
whatever you are, call back your dog, for I am a man of peace."

"Choto, Choto!..."

Golfin could make out the form of a large, black dog coming towards
him, but after sniffing round him it retired at its master's call;
and at that moment the traveller could distinguish a figure, a man,
standing as immovable as a stone image, at about ten paces below him,
on a slanting pathway which seemed to cut across the steep incline.
This path, and the human form standing there, became quite clear now to
Golfin, who, looking up to the sky, exclaimed:

"Thank God! here is the mad moon at last; now we can see where we are.
I had not the faintest notion that a path existed so close to me, why,
it is quite a road. Tell me, my friend, do you know whether the mines
of Socartes are hereabout?"

"Yes, Señor, these are the mines of Socartes; but we are at some
distance from the works."

The voice which spoke thus was youthful and pleasant, with the
attractive inflection that indicates a polite readiness to be of
service. The doctor was well pleased at detecting this, and still
better pleased at observing the soft light, which was spreading through
the darkness and bringing resurrection to earth and sky, as though
calling them forth from nothingness.

"_Fiat lux!_" he said, going forward down the slope. "I feel as if I
had just emerged into existence from primeval chaos.... Indeed, my good
friend, I am truly grateful to you for the information you have given
me, and for the farther information you no doubt will give me. I left
Villamojada as the sun was setting.--They told me to go on, straight
on...."

"Are you going to the works?" asked the strange youth, without stirring
from the spot or looking up towards the doctor, who was now quite near
him.

"Yes, Señor; but I have certainly lost my way."

"Well, this is not the entrance to the mines. The entrance is by the
steps at Rabagones, from which the road runs and the tram-way that
they are making. If you had gone that way you would have reached the
works in ten minutes. From here it is a long way, and a very bad road.
We are at the outer circle of the mining galleries, and shall have to
go through passages and tunnels, down ladders, through cuttings, up
slopes, and then down the inclined plane; in short, cross the mines
from this side to the other, where the workshops are and the furnaces,
the machines and the smelting-house."

"Well, I seem to have been uncommonly stupid," said Golfin, laughing.

"I will guide you with much pleasure, for I know every inch of the
place."

Golfin, whose feet sank in the loose earth, slipping here and tottering
there, had at last reached the solid ground of the path, and his first
idea was to look closely at the good-natured lad who addressed him.
For a minute or two he was speechless with surprise.

"You!" he said, in a low voice.

"I am blind, it is true, Señor," said the boy. "But I can run without
seeing from one end to the other of the mines of Socartes. This stick I
carry prevents my stumbling, and Choto is always with me, when I have
not got Nela with me, who is my guide. So, follow me, Señor, and allow
me to guide you."


