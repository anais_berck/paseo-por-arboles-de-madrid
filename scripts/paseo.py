from parse_trees import load_trees_from_json
from medialab import crear_base_datos, paso
from random import shuffle, random

# creating Markov Chain in text & trees
def path(word, words_tree, words_path, trees):
	# Collects a list of trees to visit
	tree_index = {}
	itinerary = []
	current_step = word.capitalize() + ' '
	previous_steps = ''
	markov_decision_traces = [ ( word, 0, [word]) ]

	posibilities, dice, next_word = paso(word, words_tree, words_path)
	
	while len(itinerary) < 50 and next_word not in '.!?':
		if next_word in ',:;\)':
			current_step = current_step[:-1]
			current_step += ' '

		breath = random()
		if breath < 0.1:
			separator = '\n'
		else:
			separator = ' '
		
		current_step += (next_word + separator)
		markov_decision_traces.append(( next_word, dice, posibilities ))

		if next_word in words_tree:
			# Current word is a tree word, this step in the itinerary is 'complete'
			# Word is not yet in the index, add a tree for this word
			if next_word not in tree_index:
				# Add tree to index and remove from list of available trees
				tree_index[next_word] = trees.pop(0)

			# Retreive tree linked to this word from the index
			tree = tree_index[next_word]

			# Get a next word from the database
			word = next_word
			posibilities, dice, next_word = paso(word, words_tree, words_path)

			# Try to look ahead to the next word, if the next word 
			# is interpunction, add it to the current step
			# but first remove trailing space
			if next_word in '.,:;!?\)':
				current_step = current_step[:-1] + next_word + ' '
				# Request a new next word to continue generation  
				markov_decision_traces.append(( next_word, dice, posibilities ))

				# Test whether the next word marks the end of a sentence,
				# thus the end of the itinerary. Then don't touch it so the
				# while will break.
				if next_word not in '.!?':
					word = next_word
					posibilities, dice, next_word = paso(word, words_tree, words_path)


			# Add the current step, and the tree to the itinerary
			itinerary.append((
				current_step, 
				previous_steps,
				tree,
				markov_decision_traces
			))

			previous_steps += current_step

			# Clear the current step
			current_step = ''
			markov_decision_traces = []
		else:
			word = next_word
			posibilities, dice, next_word = paso(word, words_tree, words_path)

	return itinerary

# Genera un camino a partir de un texto y una palabra del texto
def crear_camino(nombre_archivo, palabra_inicial, idioma='es'):
	trees = load_trees_from_json()
	shuffle(trees)
	
	#print("Starting to read text")
	(palabras_arboles, palabras_camino) = crear_base_datos(nombre_archivo, idioma)
	
	#print("Amount of tree words: ", len(palabras_arboles))

	return path(palabra_inicial, palabras_arboles, palabras_camino, trees)

if __name__ == '__main__':
	import os.path

	basepath = os.path.dirname(__file__)

	#EJECUCIÓN__________________________________________________________________
	print('Puedes elegir una novela para crear tu Paseo por árboles de Madrid.')
	print('Opción 1: La novela "La madre naturaleza" de la escritora feminista Emilia Pardo Bazán \
	fue publicada en 1887. Usa en esta obra una prosa poética y descriptiva, y en sus páginas se \
	siente el amor que profesa al paisaje gallego, con un conocimiento de la botánica y de \
	las costumbres rurales muy superior al de sus contemporáneos.')
	print('Opción 2: La novela "Miau" del escritor Benito Pérez Galdós fue publicada en 1888. \
	Enmarcada en el género realista, satiriza el Madrid burocrático de finales del siglo XIX \
	a partir de las vicisitudes vitales de su protagonista, Ramón Villaamil, \
	un competente exempleado del Ministerio de Hacienda, al que una serie de intrigas \
	han dejado cesante.')
	novel = input('Por favor, marca 1 o 2: ')

	first_word = 'un'
	if novel == '1':
		novel = os.path.join(basepath, '../data/emilia_prueba.txt')
		author = 'Emilia Pardo Bazán'
		title = 'La Madre Naturaleza'
	else:
		novel = os.path.join(basepath, '../data/prueba.txt')
		author = 'Benito Pérez Gáldos'
		title = 'Miau'

	# Create title/subtitle
	print('\nPaseo por los árboles de Madrid con', author, 'y', title, '\n')
	print('-------------------------------------------------------------------------------------------\n')

	# Create chapters
	path = crear_camino(novel, first_word)

	sentences = []
	for sentence, concatenated_steps, tree, traces in path:
		for word, dice, options in traces:
			print('Dice rolled - {} -'.format(dice))
			print('New word - {} - chosen from {}'.format(word, options))
			print('')
		sentences.append(sentence)
		print('Itinerary:\n{} \n'.format(''.join(sentences)))
		print('Tree linked to last word :', tree['properties']['NOMBRE_COMUN'], ' en ', tree['properties']['MINTDIRECCIONAUX'], '\n')
		print('\n')

